# Individual Project 2

Containerize a Rust Actix Web Service. This involves building a web app for a simple Russian Roulette game.
The player can choose a number between 0 and 30 and choose whatever amount of bet they want. Then the app will generate a random number between 0 and 30.
- If the user's chosen number is exactly the same as the generated number, the user wins three times their bet amount with a sentence "You hit the exact number! You win `The prize`!"
- If the chosen number is off by one (either one more or one less than the generated number), the user wins twice their bet amount with a sentence "So close! You win `The prize`!"
- If the difference between the chosen number and the generated number is two or three, the user wins their bet amount back with a sentence "Not bad! You win `The prize`."
- For any other difference, the user loses their bet with a sentence "You lose. Better luck next time."

## Components

- **Demo Vedio**: Showing my app running
[Watch the video](https://youtu.be/okBClsSAsj4)


- **Docker Image**: The package that contains the web app and all its dependencies.
  ![Docker Image](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG82.jpg)

- **Docker Container**: The running instance of the Docker image.
  ![Docker Container](https://raw.githubusercontent.com/LeoZhangzaolin/photos/main/WechatIMG83.jpg)

## Preparation

1. Run `cargo new individualproject2`
2. Add dependencies into `Cargo.toml`
3. Build the web app and run it using `cargo run` to test if it works
4. Create a `Dockerfile`
5. Build the Docker image using `docker build -t russian_roulette_app .`
6. Deploy the Docker container to make sure it works: `docker run -d -p 50505:50505 russian_roulette_app`