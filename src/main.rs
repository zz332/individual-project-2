use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Bet {
    chosen_number: i32,
    bet_amount: i32,
}

async fn russian_roulette(bet: web::Json<Bet>) -> impl Responder {
    if bet.chosen_number < 0 || bet.chosen_number > 30 {
        return HttpResponse::BadRequest().body("Invalid chosen number. Choose between 0 and 30.");
    }

    let generated_number = rand::thread_rng().gen_range(0..=30);
    let difference = (bet.chosen_number - generated_number).abs();

    let result = match difference {
        0 => format!("You hit the exact number! You win {}!", bet.bet_amount * 3),
        1 => format!("So close! You win {}!", bet.bet_amount * 2),
        2 | 3 => format!("Not bad! You win {}.", bet.bet_amount),
        _ => "You lose. Better luck next time.".to_string(),
    };

    HttpResponse::Ok().body(format!("Number was {}: {}", generated_number, result))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/russian-roulette", web::post().to(russian_roulette))
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind("0.0.0.0:50505")?
    .run()
    .await
}
